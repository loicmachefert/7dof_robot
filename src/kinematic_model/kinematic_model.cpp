#include "kinematic_model.h"
#include <iostream>
using namespace std;

RobotModel::RobotModel(const double &l1In, const double &l2In):
  l1  (l1In),
  l2  (l2In)

{};

void RobotModel::FwdKin(Eigen::Vector2d &xOut, Eigen::Matrix2d &JOut, const Eigen::Vector2d & qIn){
  // TODO Implement the forward and differential kinematics
  xOut(0) = l1*cos(qIn(0)) + l2*cos(qIn(0) + qIn(1)); // Compute the x position of the kinematic model.
  xOut(1) = l1*sin(qIn(0)) + l2*sin(qIn(0) + qIn(1)); // Compute the y position of the kinematic model.

  JOut << -l1*sin(qIn(0)) - l2*sin(qIn(0)+qIn(1)), -l2*sin(qIn(0)+qIn(1)), //  Compute the Jacobian matrix.
          l1*cos(qIn(0)) + l2*cos(qIn(0)+qIn(1)), l2*cos(qIn(0)+qIn(1));
}