#include "trajectory_generation.h"
#include <eigen3/Eigen/Geometry> 
using namespace Eigen;

Polynomial::Polynomial(){};

Polynomial::Polynomial(const double &piIn, const double &pfIn, const double & DtIn){
  //TODO initialize the object polynomial coefficients
  pi = piIn;
  pf = pfIn;
  Dt = DtIn;
  
  a[0] = pi;
  a[1] = 0;
  a[2] = 0;
  a[3] = 10*(pf-pi);
  a[4] = -15*(pf-pi);
  a[5] = 6*(pf-pi);
};

void          Polynomial::update(const double &piIn, const double &pfIn, const double & DtIn){
  //TODO update polynomial coefficients
  pi = piIn;
  pf = pfIn;
  Dt = DtIn;
  
  a[0] = pi;
  a[1] = 0;
  a[2] = 0;
  a[3] = 10*(pf-pi);
  a[4] = -15*(pf-pi);
  a[5] = 6*(pf-pi);
};

const double  Polynomial::p     (const double &t){
  //TODO compute position
  double p = a[0] + a[3]*pow(t/Dt,3) + a[4]*pow(t/Dt,4) + a[5]*pow(t/Dt,5);
  return p;
};

const double  Polynomial::dp    (const double &t){
  //TODO compute velocity
  double dp = 3*a[3]*pow(t/Dt,2)*(1/Dt) + 4*a[4]*pow(t/Dt,3)*(1/Dt) + 5*a[5]*pow(t/Dt,4)*(1/Dt);
  return dp;
};

Point2Point::Point2Point(const Eigen::Affine3d & X_i, const Eigen::Affine3d & X_f, const double & DtIn){
  //TODO initialize object and polynomials
  
  Eigen::Vector3d pi;
  Eigen::Vector3d pf;
  Eigen::Matrix3d Ri;
  Eigen::Matrix3d Rf;
  Eigen::Matrix3d Rif;

  X_i.linear().matrix() == Ri;
  X_i.translation() == pi;
  X_f.linear().matrix() == Rf;
  X_f.translation() == pf;

  Rif = Ri.transpose()*Rf;
  Eigen::AngleAxisd Rif2(Rif);
  Rif2.angle();
  Rif2.axis();
  double angle = 0;
  Eigen::Vector3d axis  = (0,0,0);

  Matrix3d R = AngleAxisd(angle,axis).matrix();


  polx = Polynomial(X_i(0,3),X_f(0,3),DtIn);
  poly = Polynomial(X_i(1,3),X_f(1,3),DtIn);
  polz = Polynomial(X_i(2,3),X_f(2,3),DtIn);
  pol_angle = Polynomial(Ri,Rf,DtIn);


}

Eigen::Affine3d Point2Point::X(const double & time){
  //TODO compute cartesian position
  Eigen::Affine3d X;
  X(0,3) = polx.p(time);
  X(1,3) = poly.p(time);
  X(2,3) = polz.p(time);
  
  return X;
}

Eigen::Vector6d Point2Point::dX(const double & time){
  //TODO compute cartesian velocity
  Eigen::Vector6d dX;
  dX(0) = polx.dp(time);
  dX(1) = poly.dp(time);
  dX(2) = polz.dp(time);

  return dX;
}

