#include "control.h"
#include <iostream>
Controller::Controller(RobotModel &rmIn):
  model  (rmIn)
{

}

Eigen::Vector2d Controller::Dqd ( 
          const Eigen::Vector2d & q,
          const Eigen::Vector2d & xd,
          const Eigen::Vector2d & Dxd_ff
                      )
{  
  //TODO Compute joint velocities able to track the desired cartesian position
  
  model.FwdKin(X,J,q);
  
  dX_desired = kp*(xd - X) + Dxd_ff; // Computes the desired cartesian velocity.
  
  return J.inverse()*dX_desired; // Computes and return the joint velocities.
}
