#include "control.h"
#include "trajectory_generation.h"
#include <iostream>
using namespace std;

int main(){
  // Show using all three libraries, simulate the motion of a planar robot
  // For a initial position
  Eigen::Vector2d q; // Define the type of the variable q.
  Eigen::Vector2d X_f; // Define the type of the variable X_f.
  Eigen::Matrix2d J; // Define the type of the variable J.
  Eigen::Vector2d X; // Define the type of the variable X.
  Eigen::Vector2d xd; // Define the type of the variable xd.
  Eigen::Vector2d X_i; // Define the type of the variable X_i.
  Eigen::Vector2d Dxd_ff; // Define the type of the variable Dxd_ff.
  Eigen::Vector2d e; // Define the type of the variable e.

  double Dt = 10; // Define the type and the value of the variable Dt.
  double dt = 0.001; // Define the type and the value of the variable dt.

  q << M_PI_2, M_PI_4;
  
  double l1; // Define the type of the variable l1.
  double l2;// Define the type of the variable l2.
  l1  = 0.4, l2 = 0.5;
  
  // simulate a motion achieving
  RobotModel model = RobotModel(l1, l2);
  Controller controller = Controller(model);
  Point2Point traj = Point2Point(X_i, X_f, Dt);
  

  X_f(0) = 0.0; // Define the value of the variable X_f.
  X_f(1) = 0.6;
  model.FwdKin(X_i,J,q); // Define the value of the variable X_i using q.


  for (float t = 0; t<Dt+dt; t+=dt)
  {
    xd = traj.X(t); // Computes the desired trajectory.
    cout << "xd : " << xd << "\n\n";

    model.FwdKin(X,J,q); // Computes the real trajectory.
    cout << "X : " << X << "\n\n";

    Dxd_ff = traj.dX(t);

    q = controller.Dqd(q, xd, Dxd_ff)*dt; // Computes the real angular position considered equal to the desired one.
    
  }
  
}