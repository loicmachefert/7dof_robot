#include "trajectory_generation.h"
#include <iostream>
using namespace std;

int main(){

  // Compute the trajectory for given initial and final positions. 
  Eigen::Affine3d X_i;
  Eigen::Affine3d X_f;
  

  double DtIn = 10; // Define the type and the value of the variable DtIn.
  double dt = 1e-6; // Define the type and the value of the variable dt.

  Point2Point traj = Point2Point(X_i, X_f, DtIn);
  Eigen::Affine3d pos;  // Define the type of the value pos.
  Eigen::Vector6d VXA;  // Define the type of the value VXA (Analytic velocity).
  Eigen::Vector6d VXN;  // Define the type of the vaiable VXN (Numeric check).
  
  // Display some of the computed points
  // Check numerically the velocities given by the library 
  // Check initial and final velocities
  for (float t = 0; t<DtIn+0.1; t+=0.1) // Computes the position and the velocity of the robot.
  {
    pos = traj.X(t);
    cout << "X :" << pos << "\n\n";
  
    VXA = traj.dX(t);
    cout << "VXA :" << VXA << "\n\n";
    
    VXN = (traj.X(t+dt)-traj.X(t))/dt;
    cout << "VXN :" << VXN << "\n\n";
  
 
}
